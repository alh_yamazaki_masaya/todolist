<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
	<meta charset="utf-8">
	<title>Home画面</title>
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body>
	<div class="main-contents">

	<script type="text/javascript">
		document.getElementById("view_time").innerHTML = getNow();

		function isTimeOver() {
			var now = new Date.now();
			console.log(now);

			//出力用
			return s;
		}
		</script>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="showList">
			<c:if test="${not empty todoLists}">
				<c:forEach items="${todoLists}" var="todoList">
					<c:if test="${todoList.isCompleted ==0 || isHidden !=0}">

						<c:if test="${todoList.isCompleted ==0}">
							<div class="notCompleted">
								<div class="item">
									<form:form modelAttribute="todoForm"
										action="${pageContext.request.contextPath}/isCompleted/${todoList.id}/">
										<input type="image" src="${pageContext.request.contextPath}/resources/img/no_check.png" >
									</form:form>
								</div>

								<div class="item">
									<c:out value="${todoList.text }" />
									<a href="${pageContext.request.contextPath}/edit/${todoList.id}/">編集</a>
									<br />
									<c:if test="${ not empty todoList.date}">
										<fmt:formatDate value="${todoList.date }"
											pattern="yyyy/MM/dd HH:mm" />
										<br />
									</c:if>
									<c:if test="${ not empty todoList.memo}">
										<c:out value="${todoList.memo }" />
										<br />
									</c:if>
								</div>

								<div class="item">
									<form:form modelAttribute="todoForm"
										action="${pageContext.request.contextPath}/delete/${todoList.id}/">
										<input type="submit" value="削除">
									</form:form>
								</div>
							</div>
						</c:if>
						<c:if test="${todoList.isCompleted ==1}">
							<div class="completed">
								<div class="item">
									<form:form modelAttribute="todoForm"
										action="${pageContext.request.contextPath}/isNotCompleted/${todoList.id}/">
										<input type="image" src="${pageContext.request.contextPath}/resources/img/check.png" >
									</form:form>
								</div>

								<div class="item">
									<c:out value="${todoList.text }" />
									<a href="${pageContext.request.contextPath}/edit/${todoList.id}/">編集</a>
									<br />
									<c:if test="${ not empty todoList.date}">
										<fmt:formatDate value="${todoList.date }"
											pattern="yyyy/MM/dd HH:mm:ss" />
										<br />
									</c:if>
									<c:if test="${ not empty todoList.memo}">
										<c:out value="${todoList.memo }" />
										<br />
									</c:if>
								</div>

								<div class="item">
									<form:form modelAttribute="todoForm"
										action="${pageContext.request.contextPath}/delete/${todoList.id}/">
										<input type="submit" value="削除">
									</form:form>
								</div>
							</div>
						</c:if>
					</c:if>
				</c:forEach>
			</c:if>
		</div>

		<div class="addList">
			<form:form modelAttribute="todoForm"
				action="${pageContext.request.contextPath}/regist/">
				<input id="text" name="text" type="text">
				<input type="submit" value="登録" />
			</form:form>
		</div>

		<div class="addList">
			<form:form method="GET" action="${pageContext.request.contextPath}/hidden/">
				<input type="submit" value="表示、非表示の切り替え"/>
			</form:form>
		</div>
		<div class="copyright">Copyright(c)Yamazaki Masaya</div>
	</div>

</body>
</html>
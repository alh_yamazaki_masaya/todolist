package jp.co.todolist.mapper;

import java.util.List;

import jp.co.todolist.dto.TodoListDto;
import jp.co.todolist.entity.TodoList;

public interface TodoListMapper {

	void insertTodoList(TodoListDto todoListDto);

	List<TodoList> getTodoLists();

	TodoList getTodoList(int id);

	void updateTodoList(TodoListDto todoListDto);

	void deleteTodoList(int id);

	void isCompleted(int id);

	void isNotCompleted(int id);

}

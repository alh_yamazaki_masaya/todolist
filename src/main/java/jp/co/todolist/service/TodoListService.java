package jp.co.todolist.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.todolist.dto.TodoListDto;
import jp.co.todolist.entity.TodoList;
import jp.co.todolist.mapper.TodoListMapper;

@Service
public class TodoListService {

	@Autowired
	private TodoListMapper todoListMapper;

	public void insertTodoList(TodoListDto todoListdto) {

		todoListMapper.insertTodoList(todoListdto);
	}

	public List<TodoListDto> getTodoLists() {

		List<TodoList> todoLists = todoListMapper.getTodoLists();

		List<TodoListDto> todoListDtos = convertToDto(todoLists);

		return todoListDtos;
	}

	public TodoListDto getTodoList(int id) {

		TodoList todoList = todoListMapper.getTodoList(id);
		TodoListDto todoListDto = new TodoListDto();

		BeanUtils.copyProperties(todoList, todoListDto);

		return todoListDto;
	}

	public void updateTodoList(TodoListDto todoListdto) {

		todoListMapper.updateTodoList(todoListdto);
	}


	private List<TodoListDto> convertToDto(List<TodoList> todoLists){
		List<TodoListDto> todoListDtos = new LinkedList<TodoListDto>();

		for(TodoList todolist:todoLists) {
			TodoListDto dto = new TodoListDto();

			BeanUtils.copyProperties(todolist, dto);
			todoListDtos.add(dto);
		}

		return todoListDtos;
	}

	public void deleteTodoList(int id) {

		todoListMapper.deleteTodoList(id);
	}

	public void isCompleted(int id) {

		todoListMapper.isCompleted(id);
	}

	public void isNotCompleted(int id) {

		todoListMapper.isNotCompleted(id);
	}



}

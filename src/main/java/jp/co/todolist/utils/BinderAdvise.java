package jp.co.todolist.utils;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
@Component
class BinderAdvise {

//    @InitBinder
//    fun initBinder(binder: WebDataBinder) {
//        // 日付文字列をDateにバインドする設定
//        val dateFormat: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
//        dateFormat.setLenient(false)
//        binder.registerCustomEditor(Date::class.java, "startDate", CustomDateEditor(dateFormat, true))
//        binder.registerCustomEditor(Date::class.java, "endDate", CustomDateEditor(dateFormat, true))
//
//        // 日時文字列をDateにバインドする設定
//        val datetimeFormat: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//        datetimeFormat.setLenient(false)
//        binder.registerCustomEditor(Date::class.java, "targetDatetime", CustomDateEditor(datetimeFormat, true))
//    }
}
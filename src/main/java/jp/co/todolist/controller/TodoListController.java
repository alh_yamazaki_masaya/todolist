package jp.co.todolist.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.todolist.dto.TodoListDto;
import jp.co.todolist.form.TodoForm;
import jp.co.todolist.service.TodoListService;

@Controller
public class TodoListController {

	@Autowired
	private TodoListService todoListService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
//	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
//	    dateFormat.setLenient(false);
//	    binder.registerCustomEditor(Date.class, "todoForm.date", new CustomDateEditor(dateFormat, true));
	}

	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	public String registTodoList(@ModelAttribute TodoForm form, Model model) {

		TodoListDto todoListDto = new TodoListDto();
		BeanUtils.copyProperties(form, todoListDto);
		todoListService.insertTodoList(todoListDto);

		//		List<TodoListDto> todoListDtos = new ArrayList<TodoListDto>();
		//
		//		todoListDtos = todoListService.getTodoLists();
		//		model.addAttribute("todoLists", todoListDtos);
		return "redirect:/home/";
	}

	@RequestMapping(value = "/edit/{id}/", method = RequestMethod.GET)
	public String editTodoList(Model model, @PathVariable int id) {
		TodoListDto todoListDto = todoListService.getTodoList(id);

		TodoForm form = new TodoForm();
		BeanUtils.copyProperties(todoListDto, form);
		model.addAttribute("todoForm", form);
		return "edit";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editTodoList(@Validated TodoForm form, BindingResult result,
			Model model) {
		 if (result.hasErrors()) {
			 System.out.println(result);
		 }
		TodoListDto todoListDto = new TodoListDto();
		BeanUtils.copyProperties(form, todoListDto);
		todoListService.updateTodoList(todoListDto);

		return "redirect:/home/";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public String deleteTodoList(Model model, @PathVariable int id) {

		todoListService.deleteTodoList(id);

		return "redirect:/home/";
	}

	@RequestMapping(value = "/isCompleted/{id}", method = RequestMethod.POST)
	public String isCompleted(Model model, @PathVariable int id) {

		todoListService.isCompleted(id);

		return "redirect:/home/";
	}

	@RequestMapping(value = "/isNotCompleted/{id}", method = RequestMethod.POST)
	public String isNotCompleted(Model model, @PathVariable int id) {

		todoListService.isNotCompleted(id);
		return "redirect:/home/";
	}
}

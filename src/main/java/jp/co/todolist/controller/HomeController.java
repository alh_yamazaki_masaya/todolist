package jp.co.todolist.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.todolist.dto.TodoListDto;
import jp.co.todolist.service.TodoListService;

@Controller
public class HomeController {

	@Autowired
	private TodoListService todoListService;

	@Autowired
	private HttpSession session;



	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String showTodoList(Model model) {

		List<TodoListDto> todoListDtos = new ArrayList<TodoListDto>();

		todoListDtos = todoListService.getTodoLists();
		model.addAttribute("todoLists", todoListDtos);

		int isHidden = 0;
		try {
			isHidden = (Integer) session.getAttribute("isHidden");
		} catch (Exception e) {

		}

		session.setAttribute("isHidden", isHidden);

		return "home";
	}

	@RequestMapping(value = "/hidden", method = RequestMethod.GET)
	public String changeHidden(Model model) {

		List<TodoListDto> todoListDtos = new ArrayList<TodoListDto>();

		todoListDtos = todoListService.getTodoLists();
		model.addAttribute("todoLists", todoListDtos);


		int isHidden = (Integer)session.getAttribute("isHidden");
		if(isHidden == 0) {
			isHidden = 1;
		}else {
			isHidden = 0;
		}
		session.setAttribute("isHidden",isHidden);

		return "home";
	}

}

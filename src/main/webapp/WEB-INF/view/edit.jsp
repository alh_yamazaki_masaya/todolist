<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta charset="utf-8">
<script type="text/javascript">
	function replace(date) {
		var result = date.replace(" ", "T");
		console.log(result);
		return result
	}
</script>

<title>編集画面</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="editItems">
			<form:form modelAttribute="todoForm">
				<p>内容：<form:input path="text" value="${todoForm.text}" /></p>
				<p>日時：<form:input type="datetime-local" step="600" path="date" value="replace(${todoForm.date})" /></p>
				<p>メモ：<form:input path="memo" value="${todoForm.memo}" /></p>
				<p>	重要度：<form:input path="importance" value="${todoForm.importance}" /></p>
				<input type="submit" value="更新" />
			</form:form>
		</div>

		<div class="copyright">Copyright(c)Yamazaki Masaya</div>
	</div>

</body>
</html>